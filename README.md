# Simple web scraper application
The project is Laravel 8.0 .PHP version is required ^7.3.*.

For scraping used fabpot/goutte package.

Goutte is a screen scraping and web crawling library for PHP.

Goutte provides a nice API to crawl websites and extract data from the HTML/XML responses.

## Installation

After clone project run

```bash
composer update
```

## Usage

```
php artisan serve
```


## License
[MIT](https://choosealicense.com/licenses/mit/)