<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client;


class IndexController extends Controller
{
     /**
     * @return view
     */
     public function getIndex() {
       
        $responce = $this->getCrawler('https://videx.comesconnected.com/');
  
        return view('index', compact('responce'));        
    }
    
    
    /**
     * @param string $url
     * @return JSON representation of a value
     */
    public function getCrawler($url){
        $client = new Client();
        
        $crawler = $client->request('GET', $url);
        $result = $crawler->filter('.package')->each(function ($node) {

           return  $node->filter('ul')->each(function ($child) use ($node) {
                return  [
                    'title'=>$node->children('.header  h3')->text(),
                    'description'=>$child->children('li .package-name')->text(),
                    'price'=>$child->children('li .price-big')->text(),
                    'discount'=>($child->children('li .package-price p')->count() > 0) ? $child->children('li .package-price p')->text():0 ,
                ];
                
            }); 
        });
        
         return json_encode($result, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}
