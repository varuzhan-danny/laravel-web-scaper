<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Scrape Project</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/beautify-json.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/styles.css')}}">

    </head>
    <body class="antialiased">
        <div class="container">
            <div class="logo-wrap text-xs-center">
                <img src="{{asset('assets/images/vedex.png')}}" width="200">
            </div>
            <div class="content-title text-xs-center">
                <h2>Simple web scraper application</h2>
                <h4>Laravel 8.0 project.Get videx all products like JSON</h4>
            </div>
            <div id="json">{{$responce}}</div>
        </div>
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="{{asset('assets/js/jquery.beautify-json.js')}}"></script>
        <script src="{{asset('assets/js/script.js')}}"></script>
    </body>
</html>
